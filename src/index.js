const array = [3, 5, -4, 8, 11, 1, -1, 6]
const targetSum = 100

function twoNumberSum(array, targetSum) {
    const arsum = []
    for(i = 0; i<array.length; i++){
        for(j = i+1; j<array.length; j++){
            if(array[i]+array[j] === targetSum){
                arsum.push(array[i],array[j]);
            }
        }
        
    }
    return arsum;
}

const result = twoNumberSum(array, targetSum);

if(result[1] === undefined){
    console.log("No hay coincidencias");
}
else{
    console.log(result);
}
